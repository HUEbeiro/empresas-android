# README #

EmpresasAndroid é minha implementação do Desafio técnico para Dev Android da IOasys 

### Dependências utilizadas ###

* `"com.google.dagger:dagger:$dagger_version"` foi utilizado para injeção de dependência da aplicação;
* `"com.google.code.gson:gson:$gson_version"` foi utilizado como biblioteca de serialização de objetos JSON;
* `"com.squareup.retrofit2:retrofit:$retrofit_version"` foi utilizado como Client HTTP para chamadas na API;
* `"com.squareup.retrofit2:converter-gson:$retrofit_version"` foi utilizado como ponte da biblioteca de serialização e a biblioteca de Client HTTP;
* `"com.squareup.okhttp3:logging-interceptor:$okhttp_version"` foi utilizado como biblioteca de interceptação das chamadas HTTP e também como logger dessas chamadas;
* `"com.github.bumptech.glide:glide:$glide_version"` foi utilizado para carregamento e chaching de imagens na aplicação;
* `"androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"` foi utilizado a implementação de praticas Jetpack em conjunto à arquitetura MVVM, em específico a camada ViewModel;
* `"androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle_version"` foi utilizado para a implementação de LiveData na aplicação.

### Se houvesse mais tempo ###

Se houvesse mais tempo para desenvolvimento do teste:

* Seria feito a implementação de testes unitários e também testes de instrumentação (fazendo uso da biblioteca Espresso): para automatizar todo o processo de teste que será executado durante a avaliação;
* Seria implementado um tipo base para o Adapter das RecyclerViews (tirando algumas responsabilidades da classe EnterpriseDataAdapter e pensando no crescimento da aplicação);
* Seria modelado uma tela de splash fazendo uso do icone disponibilizado `logo_ioasys.png`.
* Seria implementado um meio de persistência da autenticação do usuário e as informações OAuth (por SharedPreferences ou RoomDatabase se o volume de informações for crescer).

### Como executar a aplicação ###

O projeto deste repositório é um projeto compatível com a IDE Android Studio. E para executá-lo na IDE basta rodá-lo na opção "Run" e a aplicação será instalada no dispositivo (em uma AVD ou um dispositivo físico).
Após esse processo você será apresentado para a tela de autenticação, onde basta entrar com as informações de teste:
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234

