package com.huebeiro.empresasioasys.module.base

import com.huebeiro.empresasioasys.MainApp
import com.huebeiro.empresasioasys.module.EnterpriseModule
import com.huebeiro.empresasioasys.module.UserModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    UserModule::class,
    EnterpriseModule::class
])
interface ApplicationComponent {
    fun inject(target: MainApp)
}