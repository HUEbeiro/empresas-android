package com.huebeiro.empresasioasys.module

import com.huebeiro.empresasioasys.module.base.BaseApi
import com.huebeiro.empresasioasys.request.UserService
import dagger.Module
import dagger.Provides

/**
 * The Dagger module that will inject UserService
 */
@Module
class UserModule : BaseApi() {

    @Provides
    fun provideApiService(): UserService {
        return provideRetrofit(
            apiURL,
            provideClient()
        ).create(UserService::class.java)
    }
}