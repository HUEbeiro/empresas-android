package com.huebeiro.empresasioasys.module.base

import com.google.gson.GsonBuilder
import com.huebeiro.empresasioasys.MainApp
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.IllegalStateException

/**
 * The abstract base class for API related Modules
 */
@Module
abstract class BaseApi {
    private val host = MainApp.hostUrl
    private val apiVersion = "v1"
    protected val apiURL = "$host/api/$apiVersion/"
    private val httpLogging = true

    /**
     * Builds and provides the instance of a OkHttpClient for Retrofit's usage
     * @param requiresOAuth A flag indicating whether or not the API endpoint requires OAuth headers on its calls
     */
    @Provides
    fun provideClient(requiresOAuth: Boolean = false): OkHttpClient {

        val builder = OkHttpClient.Builder()

        //Adding logging interceptor if needed
        if(httpLogging) {
            val logInterceptor = HttpLoggingInterceptor()
            logInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(logInterceptor)
        }

        //Creating an header interceptor for adding default and OAuth request headers
        val headerInterceptor = Interceptor { chain: Interceptor.Chain? ->
            if(chain == null) {
                throw IllegalStateException("Interceptor chain cannot be null.")
            }

            val original = chain.request()

            val requestBuilder = original.newBuilder()
                .header("Content-Type", "application/json")

            if(requiresOAuth){
                if(MainApp.oAuthInfo == null){
                    throw IllegalStateException("OAuth Info is not set.")
                }

                requestBuilder.header("access-token", MainApp.oAuthInfo!!.accessToken)
                requestBuilder.header("client", MainApp.oAuthInfo!!.client)
                requestBuilder.header("uid", MainApp.oAuthInfo!!.uid)
            }

            //Making sure the original request is present
            requestBuilder
                .method(original.method, original.body)

            chain.proceed(requestBuilder.build())

        }
        builder.addInterceptor(headerInterceptor)

        return builder.build()
    }

    /**
     * Builds and provides a Retrofit instance for API calls
     * @param baseUrl the URL address for the API
     * @param client the OkHttpClient with the request
     */
    @Provides
    fun provideRetrofit(baseUrl: String, client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(
                GsonConverterFactory.create( //Adding GSON
                    GsonBuilder().create()
                )
            )
            .build()
}