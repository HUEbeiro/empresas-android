package com.huebeiro.empresasioasys.module

import com.huebeiro.empresasioasys.module.base.BaseApi
import com.huebeiro.empresasioasys.request.EnterpriseService
import dagger.Module
import dagger.Provides

/**
 * The Dagger module that will inject EnterpriseService
 */
@Module
class EnterpriseModule : BaseApi() {

    @Provides
    fun provideApiService(): EnterpriseService {
        return provideRetrofit(
            apiURL,
            provideClient(requiresOAuth = true) //Making sure OAuth information will be sent to API
        ).create(EnterpriseService::class.java)
    }
}