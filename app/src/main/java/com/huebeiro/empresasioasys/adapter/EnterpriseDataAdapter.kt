package com.huebeiro.empresasioasys.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.huebeiro.empresasioasys.R
import com.huebeiro.empresasioasys.databinding.EnterpriseItemBinding
import com.huebeiro.empresasioasys.model.Enterprise
import com.huebeiro.empresasioasys.view.HomeActivity

/**
 * Recycler View Data Adapter for the Enterprise model class
 */
class EnterpriseDataAdapter(
    private val homeActivity: HomeActivity
) : RecyclerView.Adapter<EnterpriseDataAdapter.EnterpriseViewHolder>() {

    private var enterprises: List<Enterprise>? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): EnterpriseViewHolder {
        val enterpriseListItemBinding: EnterpriseItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(viewGroup.context),
            R.layout.enterprise_item, viewGroup, false
        )
        //Inflating the enterprise item layout via DataBinding
        return EnterpriseViewHolder(enterpriseListItemBinding)
    }

    override fun onBindViewHolder(enterpriseViewHolder: EnterpriseViewHolder, i: Int) {
        val enterprise: Enterprise? = enterprises?.get(i)
        enterpriseViewHolder.enterpriseItemBinding.view = homeActivity
        if(enterprise != null)
            enterpriseViewHolder.enterpriseItemBinding.enterprise = enterprise
    }

    override fun getItemCount(): Int {
        return enterprises?.size ?: 0
    }

    fun setEnterpriseList(enterprises: List<Enterprise>?) {
        this.enterprises = enterprises
        notifyDataSetChanged()
    }

    class EnterpriseViewHolder(enterpriseBinding: EnterpriseItemBinding) :
        RecyclerView.ViewHolder(enterpriseBinding.root) {
        val enterpriseItemBinding: EnterpriseItemBinding = enterpriseBinding
    }
}