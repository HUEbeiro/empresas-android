package com.huebeiro.empresasioasys.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.View
import com.huebeiro.empresasioasys.MainApp
import com.huebeiro.empresasioasys.R
import com.huebeiro.empresasioasys.databinding.SigninBinding
import com.huebeiro.empresasioasys.model.OAuthInfo
import com.huebeiro.empresasioasys.model.SignInResponse
import com.huebeiro.empresasioasys.viewmodel.SignInViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * The activity that allows the user to sign into the application
 */
class SignInActivity : AppCompatActivity() {

    lateinit var viewModel : SignInViewModel
    lateinit var binding : SigninBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = SignInViewModel()
        binding = DataBindingUtil.setContentView(this, R.layout.signin)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.view = this
    }

    fun onSignInClick(){
        hideErrorMessage()
        if(viewModel.textEmail.value?.isNotBlank() != true ||
            viewModel.textPassword.value?.isNotBlank() != true) {
            showErrorMessage()
            return
        }
        viewModel.startLoading()
        viewModel.signIn(signInCallBack)
    }


    /**
     * The callback to the API call that retrieves the user information
     */
    private val signInCallBack = object : Callback<SignInResponse> {

        override fun onResponse(
            call: Call<SignInResponse>,
            response: Response<SignInResponse>
        ) {
            viewModel.stopLoading()
            if(response.isSuccessful &&
                response.body() != null &&
                response.body()!!.success &&
                OAuthInfo.hasInfo(response)) {
                completeSignIn(response)
                return
            }

            showErrorMessage()
        }

        override fun onFailure(call: Call<SignInResponse>, t: Throwable) {
            viewModel.stopLoading()
            showErrorMessage()
        }

    }

    private fun hideErrorMessage(){
        binding.layoutEmail.error = null
        binding.layoutPassword.error = null
        viewModel.errorVisibility.value = View.GONE
    }

    private fun showErrorMessage(){
        binding.layoutEmail.error = binding.textError.error
        binding.layoutPassword.error = binding.textError.error
        viewModel.errorVisibility.value = View.VISIBLE
    }

    private fun completeSignIn(response: Response<SignInResponse>){
        //Sending the retrieved info to the main application's companion
        MainApp.setUserInfo(
            response.body()!!.investor,
            OAuthInfo(response)
        )
        startHomeActivity()
    }

    private fun startHomeActivity(){
        startActivity(
            Intent(this, HomeActivity::class.java)
        )
        finish()
    }
}