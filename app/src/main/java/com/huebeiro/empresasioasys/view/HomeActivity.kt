package com.huebeiro.empresasioasys.view

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.huebeiro.empresasioasys.R
import com.huebeiro.empresasioasys.adapter.EnterpriseDataAdapter
import com.huebeiro.empresasioasys.databinding.HomeBinding
import com.huebeiro.empresasioasys.model.Enterprise
import com.huebeiro.empresasioasys.model.SearchEnterprisesResponse
import com.huebeiro.empresasioasys.viewmodel.HomeViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * The activity that executes enterprise searches and displays its results
 */
class HomeActivity : AppCompatActivity() {

    lateinit var viewModel : HomeViewModel
    lateinit var binding : HomeBinding
    lateinit var enterpriseDataAdapter: EnterpriseDataAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = HomeViewModel()
        binding = DataBindingUtil.setContentView(this, R.layout.home)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.view = this

        enterpriseDataAdapter = EnterpriseDataAdapter(this)
        binding.recyclerViewEnterprises.adapter = enterpriseDataAdapter
        binding.recyclerViewEnterprises.layoutManager = LinearLayoutManager(this)

        startActionBar()
    }

    private fun startActionBar() {
        //Setting the custom view with IOasys icon on the Action Bar
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.home_actionbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //Inflating the search menu view into the Options Menu
        val inflater = menuInflater
        inflater.inflate(R.menu.search_view, menu)

        val searchViewItem: MenuItem = menu!!.findItem(R.id.action_search)

        //Adding the text listeners into the search view
        startSearchView(searchViewItem.actionView as SearchView)

        return super.onCreateOptionsMenu(menu)
    }

    private fun startSearchView(searchView: SearchView) {
        searchView.setOnQueryTextListener(getTextListener(searchView))
    }

    private fun getTextListener(searchView: SearchView) = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            searchView.clearFocus() //Clearing the focus to display results

            if(query.isNullOrEmpty()){
                //If the text is empty the search is reset
                viewModel.startSearchVisibility.value = View.VISIBLE
                updateEnterprises(null)
                return true
            }

            viewModel.startSearchVisibility.value = View.GONE
            viewModel.startLoading()
            //Starting search upon the query submit
            viewModel.searchEnterprises(query, searchCallback)

            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            //Do nothing upon the search view text change
            return false
        }
    }

    /**
     * The callback to the API call that retrieves search results
     */
    private val searchCallback = object : Callback<SearchEnterprisesResponse> {
        override fun onResponse(
            call: Call<SearchEnterprisesResponse>,
            response: Response<SearchEnterprisesResponse>
        ) {
            viewModel.stopLoading()

            if(response.isSuccessful &&
                response.body() != null) {
                updateEnterprises(response.body()!!.enterprises)
                return
            }

            if(response.code() == 401) { //Invalid OAuth Info; sign-out
                signOut()
                return
            }

            showErrorMessage() //Display
        }

        override fun onFailure(call: Call<SearchEnterprisesResponse>, t: Throwable) {
            viewModel.stopLoading()
            showErrorMessage()
        }
    }

    private fun updateEnterprises(enterprises: List<Enterprise>?) {
        //Updating the data adapter's Enterprise List
        enterpriseDataAdapter.setEnterpriseList(enterprises)
        if(enterprises != null && enterprises.count() == 0){
            viewModel.notFoundVisibility.value = View.VISIBLE
        } else {
            viewModel.notFoundVisibility.value = View.GONE
        }
    }

    private fun signOut() {
        Toast.makeText(this, R.string.text_logout_error, Toast.LENGTH_LONG).show()
        startActivity(
            Intent(this, SignInActivity::class.java)
        )
        finish()
    }

    private fun showErrorMessage() {
        Toast.makeText(this, R.string.text_search_error, Toast.LENGTH_LONG).show()
    }

    fun onEnterpriseItemClick(enterpriseId: Int){
        startActivity(
            Intent(this, EnterpriseActivity::class.java)
                .putExtra(EnterpriseActivity.ENTERPRISE_ID_KEY, enterpriseId)
        )
    }
}