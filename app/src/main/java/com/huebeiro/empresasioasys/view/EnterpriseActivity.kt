package com.huebeiro.empresasioasys.view

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.TypefaceSpan
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.huebeiro.empresasioasys.R
import com.huebeiro.empresasioasys.databinding.EnterpriseBinding
import com.huebeiro.empresasioasys.model.GetEnterpriseResponse
import com.huebeiro.empresasioasys.viewmodel.EnterpriseViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Activity that retrieves and displays an Enterprise's information
 */
class EnterpriseActivity : AppCompatActivity() {

    lateinit var viewModel : EnterpriseViewModel
    lateinit var binding : EnterpriseBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Getting the enterprise ID via the Intent
        val enterpriseId = intent.getIntExtra(ENTERPRISE_ID_KEY, 0)

        if(enterpriseId == 0){ //If the enterprise ID is not passed the activity is finished
            finish()
            return
        }

        viewModel = EnterpriseViewModel()
        binding = DataBindingUtil.setContentView(this, R.layout.enterprise)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.view = this

        viewModel.startLoading()
        viewModel.updateEnterpriseInfo(enterpriseId, getEnterpriseCallback)

        //Changing the action bar title upon enterprise updates
        viewModel.enterprise.observe(this, {
            setActionBarTitle(it?.name)
        })

        startActionBar()
    }

    private fun startActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setActionBarTitle(title: String?){
        val s = SpannableString(title)
        //Type Face indicated in the Zeplin design
        val tfs = TypefaceSpan("roboto_regular.ttf")

        s.setSpan(tfs, 0, s.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        //Setting the action bar's title with the custom Type Face via the Spannable String
        supportActionBar?.title = s
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed() //When the navigation button is pressed onBackPressed is fired
        return true
    }

    /**
     * The callback to the API call that retrieves the enterprise details
     */
    private val getEnterpriseCallback = object : Callback<GetEnterpriseResponse> {
        override fun onResponse(
            call: Call<GetEnterpriseResponse>,
            response: Response<GetEnterpriseResponse>
        ) {
            viewModel.stopLoading()
            if(response.isSuccessful &&
                response.body() != null &&
                response.body()!!.success) {
                //Updating the LiveData's value with the retrieved information
                viewModel.enterprise.value = response.body()!!.enterprise
                return
            }

            showErrorMessage() //Display error if not successful response
        }

        override fun onFailure(call: Call<GetEnterpriseResponse>, t: Throwable) {
            //Display error on a failed request
            viewModel.stopLoading()
            showErrorMessage()
        }
    }

    private fun showErrorMessage(){
        Toast.makeText(this, R.string.text_enterprise_error, Toast.LENGTH_LONG).show()
        finish()
    }

    companion object {
        /**
         * Intent Extra Key for the detailed enterprise's ID
         */
        const val ENTERPRISE_ID_KEY = "enterprise_id"
    }
}