package com.huebeiro.empresasioasys.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.huebeiro.empresasioasys.MainApp
import com.huebeiro.empresasioasys.model.SignInBody
import com.huebeiro.empresasioasys.model.SignInResponse
import com.huebeiro.empresasioasys.viewmodel.base.BaseViewModel
import retrofit2.Callback

class SignInViewModel : BaseViewModel() {

    val textEmail = MutableLiveData<String>()
    val textPassword = MutableLiveData<String>()

    val errorVisibility = MutableLiveData(View.GONE)

    fun signIn(callback: Callback<SignInResponse>){

        val email = textEmail.value
        val password = textPassword.value

        //Reassuring user information
        if(!email.isNullOrEmpty() && !password.isNullOrEmpty()){
            //Making API to sign-in the user
            MainApp.mainInstance.userService.signIn(
                SignInBody(
                    email,
                    password
                )
            ).enqueue(callback)
        }
    }
}