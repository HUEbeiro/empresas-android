package com.huebeiro.empresasioasys.viewmodel.base

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.huebeiro.empresasioasys.MainApp
import com.huebeiro.empresasioasys.R

/**
 * The base class for the application ViewModels
 */
open class BaseViewModel : ViewModel() {

    /**
     * The LiveData that holds the loading layout visibility
     */
    val loadingVisibility = MutableLiveData(View.GONE)

    /**
     * Displays the loading layout on the view if there is any present
     */
    fun startLoading(){
        loadingVisibility.value = View.VISIBLE
    }

    /**
     * Hides the loading layout on the view if there is any present
     */
    fun stopLoading() {
        loadingVisibility.value = View.GONE
    }

    companion object{
        /**
         * The binding adapter method responsible to load images
         * into the Image View making use of Glide
         */
        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String?) {
            if(url.isNullOrEmpty())
                //Loading the default image if the URL is null
                Glide.with(view.context).load(R.drawable.ic_enterprise_1).into(view)
            else
                //Loading the image (with the Host URL) into the Image View
                Glide.with(view.context).load("${MainApp.hostUrl}$url").into(view)
        }
    }
}