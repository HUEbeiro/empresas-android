package com.huebeiro.empresasioasys.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.huebeiro.empresasioasys.MainApp
import com.huebeiro.empresasioasys.model.SearchEnterprisesResponse
import com.huebeiro.empresasioasys.viewmodel.base.BaseViewModel
import retrofit2.Callback

class HomeViewModel : BaseViewModel() {

    val startSearchVisibility = MutableLiveData(View.VISIBLE)
    val notFoundVisibility = MutableLiveData(View.GONE)

    fun searchEnterprises(query: String, callback: Callback<SearchEnterprisesResponse>){
        if(query.isNotEmpty()){
            MainApp.mainInstance.enterpriseService.searchEnterprises(
                query
            ).enqueue(callback)
        }
    }

}