package com.huebeiro.empresasioasys.viewmodel

import androidx.lifecycle.MutableLiveData
import com.huebeiro.empresasioasys.MainApp
import com.huebeiro.empresasioasys.model.Enterprise
import com.huebeiro.empresasioasys.model.GetEnterpriseResponse
import com.huebeiro.empresasioasys.model.OAuthInfo
import com.huebeiro.empresasioasys.viewmodel.base.BaseViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EnterpriseViewModel : BaseViewModel(){

    val enterprise = MutableLiveData<Enterprise?>()

    fun updateEnterpriseInfo(enterpriseId : Int, callback: Callback<GetEnterpriseResponse>){
        if(enterpriseId != 0){
            MainApp.mainInstance.enterpriseService.getEnterprise(
                enterpriseId
            ).enqueue(callback)
        }
    }


}