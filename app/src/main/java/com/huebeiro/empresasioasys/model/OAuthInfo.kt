package com.huebeiro.empresasioasys.model

import retrofit2.Response

/**
 * A class that holds the OAuth information for the current signed user
 * @param response The response body to the SignIn API call that contains the OAuth information
 */
class OAuthInfo(response : Response<SignInResponse>){
    val accessToken: String
    val client: String
    val uid: String
    init {
        val tempToken = response.headers()[ACCESS_TOKEN_KEY]
        val tempClient = response.headers()[CLIENT_KEY]
        val tempUid = response.headers()[UID_KEY]

        if(tempToken == null || tempClient == null || tempUid == null){
            throw IllegalArgumentException("One of the OAuth headers is not present")
        }

        accessToken = tempToken
        client = tempClient
        uid = tempUid
    }

    companion object {
        const val ACCESS_TOKEN_KEY = "access-token"
        const val CLIENT_KEY = "client"
        const val UID_KEY = "uid"

        fun hasInfo(response : Response<SignInResponse>) : Boolean{
            return response.headers().names().contains(ACCESS_TOKEN_KEY) &&
             response.headers().names().contains(CLIENT_KEY) &&
             response.headers().names().contains(UID_KEY)
        }
    }
}