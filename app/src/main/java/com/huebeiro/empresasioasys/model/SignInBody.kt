package com.huebeiro.empresasioasys.model
import com.google.gson.annotations.SerializedName

/**
 * A data class that represents the request body of a SignIn API call
 */
data class SignInBody(
    @SerializedName("email") var email: String,
    @SerializedName("password") var password: String
)
