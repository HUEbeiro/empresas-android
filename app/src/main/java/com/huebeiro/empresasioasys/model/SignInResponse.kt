package com.huebeiro.empresasioasys.model
import com.google.gson.annotations.SerializedName

/**
 * The data class that represents the response body of a SignIn API call
 */
data class SignInResponse (
	@SerializedName("investor") val investor : Investor,
	@SerializedName("enterprise") val enterprise : String,
	@SerializedName("success") val success : Boolean
)