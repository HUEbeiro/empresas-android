package com.huebeiro.empresasioasys.model
import com.google.gson.annotations.SerializedName

data class Portfolio (

	@SerializedName("enterprises_number") val enterprises_number : Int,
	@SerializedName("enterprises") val enterprises : List<String>
)