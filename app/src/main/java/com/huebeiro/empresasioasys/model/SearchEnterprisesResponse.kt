package com.huebeiro.empresasioasys.model

import com.google.gson.annotations.SerializedName
/**
 * The data class that represents the response body of a SearchEnterprises API call
 */
data class SearchEnterprisesResponse(
    @SerializedName("enterprises") val enterprises : List<Enterprise>
)