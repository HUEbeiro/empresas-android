package com.huebeiro.empresasioasys.model

import com.google.gson.annotations.SerializedName

data class Enterprise (
    @SerializedName("id") val id : Int,
    @SerializedName("enterprise_name") val name : String,
    @SerializedName("enterprise_type") val type : Type,
    @SerializedName("country") val country : String,
    @SerializedName("email_enterprise") val email : String,
    @SerializedName("facebook") val facebook : String,
    @SerializedName("twitter") val twitter : String,
    @SerializedName("linkedin") val linkedIn : String,
    @SerializedName("phone") val phone : String,
    @SerializedName("own_enterprise") val ownEnterprise : Boolean,
    @SerializedName("photo") val photo : String,
    @SerializedName("description") val description : String,
    @SerializedName("city") val city : String,
    @SerializedName("value") val value : Int,
    @SerializedName("share_price") val sharePrice : Int,
    @SerializedName("shares") val shares : Int,
    @SerializedName("own_shares") val ownShares : Int
) {

    data class Type(
        @SerializedName("id") val id : Int,
        @SerializedName("enterprise_type_name") val name : String
    )
}


