package com.huebeiro.empresasioasys.model
import com.google.gson.annotations.SerializedName

data class Investor (

	@SerializedName("id") val id : Int,
	@SerializedName("investor_name") val investorName : String,
	@SerializedName("email") val email : String,
	@SerializedName("city") val city : String,
	@SerializedName("country") val country : String,
	@SerializedName("balance") val balance : Int,
	@SerializedName("photo") val photo : String,
	@SerializedName("portfolio") val portfolio : Portfolio,
	@SerializedName("portfolio_value") val portfolioValue : Int,
	@SerializedName("first_access") val firstAccess : Boolean,
	@SerializedName("super_angel") val superAngel : Boolean
)