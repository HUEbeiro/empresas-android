package com.huebeiro.empresasioasys.model

import com.google.gson.annotations.SerializedName

/**
 * The data class that represents the response body of a GetEnterprise API call
 */
data class GetEnterpriseResponse(
    @SerializedName("enterprise") val enterprise : Enterprise,
    @SerializedName("success") val success : Boolean
)
