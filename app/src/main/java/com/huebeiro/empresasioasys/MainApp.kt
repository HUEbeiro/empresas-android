package com.huebeiro.empresasioasys

import android.app.Application
import com.huebeiro.empresasioasys.model.Investor
import com.huebeiro.empresasioasys.model.OAuthInfo
import com.huebeiro.empresasioasys.module.base.ApplicationComponent
import com.huebeiro.empresasioasys.module.base.DaggerApplicationComponent
import com.huebeiro.empresasioasys.request.EnterpriseService
import com.huebeiro.empresasioasys.request.UserService
import javax.inject.Inject

/**
 * The main class responsible of maintaining the current application state
 */
class MainApp : Application() {

    init {
        mInstance = this
        mComponent = DaggerApplicationComponent.builder()
            .build()
        mComponent.inject(this)
    }

    /***
     * The API service for user related calls
     */
    @Inject
    lateinit var userService: UserService
    /***
     * The API service for enterprise related calls
     */
    @Inject
    lateinit var enterpriseService: EnterpriseService

    companion object{
        /***
         * The Host URL for used for API calls and image resources
         */
        const val hostUrl = "https://empresas.ioasys.com.br"

        private lateinit var mInstance : MainApp
        private lateinit var mComponent : ApplicationComponent

        private var currentInvestor : Investor? = null
        private var currentOAuthInfo : OAuthInfo? = null

        /**
         * Represents the current instance of the Application
         */
        val mainInstance : MainApp
            get() = mInstance

        /***
         * Holds the instance of the signed user
         */
        val investor : Investor?
            get() = currentInvestor

        /***
         * Holds the OAuth information of the current signed user
         */
        val oAuthInfo : OAuthInfo?
            get() = currentOAuthInfo

        /***
         * Sets the user and OAuth information for future usage and API calls
         * @param investor The current user
         * @param oAuthInfo The OAuth information for the user
         */
        fun setUserInfo(investor: Investor, oAuthInfo: OAuthInfo){
            currentInvestor = investor
            currentOAuthInfo = oAuthInfo
        }
    }
}