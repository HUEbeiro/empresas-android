package com.huebeiro.empresasioasys.request

import com.huebeiro.empresasioasys.model.GetEnterpriseResponse
import com.huebeiro.empresasioasys.model.SearchEnterprisesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * The Service Interface to represent Enterprise related API calls
 */
interface EnterpriseService  {

    @GET("enterprises")
    fun searchEnterprises(@Query("name") name: String) : Call<SearchEnterprisesResponse>

    @GET("enterprises/{id}")
    fun getEnterprise(@Path("id") id: Int) : Call<GetEnterpriseResponse>

}