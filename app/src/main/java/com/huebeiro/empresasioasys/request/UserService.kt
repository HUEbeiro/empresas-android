package com.huebeiro.empresasioasys.request

import com.huebeiro.empresasioasys.model.SignInBody
import com.huebeiro.empresasioasys.model.SignInResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * The Service Interface to represent User related API calls
 */
interface UserService  {

    @POST("users/auth/sign_in")
    fun signIn(@Body user: SignInBody) : Call<SignInResponse>

}